﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private float hp;
    [SerializeField] private LayerMask playerMask;
    [SerializeField] private float attackCooldown;
    [SerializeField] private float damage;
    [SerializeField] private float attackRange;

    private float aTimer; //AttackCooldown timer

    public GameObject player;
    public Rigidbody2D rb;

    private void Update() 
    {
        Vector2 movement = (Vector2)player.transform.position - (Vector2)transform.position;
        if(movement.x != 0 || movement.y != 0) movement /= Mathf.Sqrt(movement.x * movement.x + movement.y * movement.y);
        rb.velocity = movement * speed;

        if(hp <= 0)
            Death();

        aTimer -= Time.deltaTime;
        RaycastHit2D hit = Physics2D.Raycast(transform.position, movement, 50.0f, playerMask);
        if(hit.distance <= attackRange && aTimer <= 0)
        {
            Debug.Log("The attack has hit");
            aTimer = attackCooldown;
            player.GetComponent<Player>().OnHit(damage);
        }
    }

    private void Death()
    {
        Debug.Log(transform.name + " has died.");
        Scores.AddScore(5);
        Destroy(gameObject);
    }

    public void OnHit(float _damage)
    {
        hp -= _damage;
    }
    
    public void OnSpawn(GameObject _player, float _hp)
    {
        player = _player;
        hp = _hp;
    }
}