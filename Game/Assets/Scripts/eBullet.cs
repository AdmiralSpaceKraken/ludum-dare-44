﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class eBullet : MonoBehaviour
{
    private float damage;

    public float deathTimer;
    public Rigidbody2D rb;

    private void OnTriggerEnter2D(Collider2D other) 
    {
        if(other.tag == "Map" || other.tag == "Bullet")
            Destroy(gameObject);
        else if(other.tag == "Player")
        {
            other.GetComponent<Player>().OnHit(damage);
            Destroy(gameObject);
        }
    }

    private void Update() 
    {
        deathTimer -= Time.deltaTime;
        if(deathTimer <= 0) Destroy(gameObject);
    }

    public void OnSpawn(Vector2 direction, float _damage, float speed)
    {
        damage = _damage;
        rb.velocity = direction * speed;
    }
}