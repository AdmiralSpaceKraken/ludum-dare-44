﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class MenuMusic : MonoBehaviour
{
    public AudioClip[] music;
    public AudioSource source;

   private void Start() 
   {
       DontDestroyOnLoad(gameObject);
       Settings.musicVolume = 1;
   }

   private void Update() 
   {
       if(SceneManager.GetActiveScene().buildIndex == 3 && source.clip == music[0]) 
       {
           source.clip = music[1];
           source.Play();
       }
       else if(SceneManager.GetActiveScene().buildIndex != 3 && source.clip == music[1])
       {
            source.clip = music[0];
            source.Play();
       }

       if(source.volume != Settings.musicVolume) source.volume = Settings.musicVolume;
   }
}