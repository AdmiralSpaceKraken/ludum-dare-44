﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scores
{
    public static int currentScore;
    public static int score1;
    public static int score2;
    public static int score3;
    public static int score4;
    public static int score5;

    public static void RotateScores()
    {
        if(currentScore >= score5)
        {
            score5 = currentScore;
        }
        
        if(currentScore >= score4)
        {
            score5 = score4;
            score4 = currentScore;
        }

        if(currentScore >= score3)
        {
            score4 = score3;
            score3 = currentScore;
        }

        if(currentScore >= score2)
        {
            score3 = score2;
            score2 = currentScore;
        }

        if(currentScore >= score1)
        {
            score2 = score1;
            score1 = currentScore;
        }
    }

    public static void AddScore(int addition)
    {
        currentScore += addition;
    }

    public static void SaveScores()
    {
        PlayerPrefs.SetInt("Score1", score1);
        PlayerPrefs.SetInt("Score2", score2);
        PlayerPrefs.SetInt("Score3", score3);
        PlayerPrefs.SetInt("Score4", score4);
        PlayerPrefs.SetInt("Score5", score5);
        PlayerPrefs.Save();
    }

    public static void LoadScores()
    {
        score1 = PlayerPrefs.GetInt("Score1");
        score2 = PlayerPrefs.GetInt("Score2");
        score3 = PlayerPrefs.GetInt("Score3");
        score4 = PlayerPrefs.GetInt("Score4");
        score5 = PlayerPrefs.GetInt("Score5");
    }
}