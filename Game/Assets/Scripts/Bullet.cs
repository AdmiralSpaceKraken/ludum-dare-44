﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private float damage;

    public float deathTimer;
    public Rigidbody2D rb;

    private void OnTriggerEnter2D(Collider2D other) 
    {
        if(other.tag == "Map" || other.tag == "Bullet")
            Destroy(gameObject);
        else if(other.tag == "Enemy")
        {
            other.GetComponent<Enemy>().OnHit(damage);
            Destroy(gameObject);
        }
        else if(other.tag == "Enemy2")
        {
            other.GetComponent<Enemy2>().OnHit(damage);
            Destroy(gameObject);
        }
    }

    private void Update() 
    {
        deathTimer -= Time.deltaTime;
        if(deathTimer <= 0) Destroy(gameObject);
    }

    public void OnSpawn(Vector2 direction, float _damage, float speed)
    {
        damage = _damage;
        rb.velocity = direction * speed;
    }
}