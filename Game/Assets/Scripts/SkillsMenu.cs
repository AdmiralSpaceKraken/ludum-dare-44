﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SkillsMenu : MonoBehaviour
{
    public Text hpText;
    public Text moveText;
    public Text speedText;
    public Text damageText;
    public Text bulletText;

    private void Awake() 
    {
        PlayerSkills.hp = 10;
        PlayerSkills.moveSpeed = 1;
        PlayerSkills.attackSpeed = 1;
        PlayerSkills.attackDamage = 1;
        PlayerSkills.bulletSpeed = 1;
    }

    private void Update() 
    {
        hpText.text = "Health: " + PlayerSkills.hp;
        moveText.text = "Movement: " + PlayerSkills.moveSpeed;
        speedText.text = "Attack Speed: " + PlayerSkills.attackSpeed;
        damageText.text = "Attack Damage: " + PlayerSkills.attackDamage;
        bulletText.text = "Projectile Speed: " + PlayerSkills.bulletSpeed;
    }

    public void ChangeMoveSpeed(int i)
    {
        if((i < 0 && PlayerSkills.moveSpeed > 1) || (i > 0 && PlayerSkills.hp > 1))
        {
            PlayerSkills.moveSpeed += i;
            PlayerSkills.hp -= i;
        }
    }

    public void ChangeAttackSpeed(int i)
    {
        if((i < 0 && PlayerSkills.attackSpeed > 1) || (i > 0 && PlayerSkills.hp > 1))
        {
            PlayerSkills.attackSpeed += i;
            PlayerSkills.hp -= i;
        }
    }

    public void ChangeAttackDamage(int i)
    {
        if((i < 0 && PlayerSkills.attackDamage > 1) || (i > 0 && PlayerSkills.hp > 1))
        {
            PlayerSkills.attackDamage += i;
            PlayerSkills.hp -= i;
        }
    }

    public void ChangeBulletSpeed(int i)
    {
        if((i < 0 && PlayerSkills.bulletSpeed > 1) || (i > 0 && PlayerSkills.hp > 1))
        {
            PlayerSkills.bulletSpeed += i;
            PlayerSkills.hp -= i;
        }
    }

    public void StartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}