﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSkills
{
    public static int hp;
    public static int moveSpeed;
    public static int attackSpeed;
    public static int attackDamage;
    public static int bulletSpeed;
}