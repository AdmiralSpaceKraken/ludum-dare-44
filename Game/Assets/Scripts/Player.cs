﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    #region SettingUpVariables

    private float shotTimer;
    private bool isPaused = false;

    public float bulletSpeed;
    public float playerSpeed;
    public float bulletDamage;
    public float timeBetweenShots;
    public float hp;
    public Rigidbody2D rb;
    public GameObject bullet;
    public GameObject pauseMenu;
    #endregion

    private void Start() 
    {
        hp = PlayerSkills.hp;
        playerSpeed = 6.0f + 0.5f * PlayerSkills.moveSpeed;
        timeBetweenShots = 0.60f - 0.05f * PlayerSkills.attackSpeed;
        bulletDamage = 1 + 0.5f * PlayerSkills.attackDamage;
        bulletSpeed =  7.5f + 1.0f * PlayerSkills.bulletSpeed;
    }

    void Update()
    {
        PlayerMove();

        #region  playerShooting
        if(shotTimer <= 0)
        {
            if(Input.GetKey(KeyCode.LeftArrow))
                Shoot(new Vector2(-1, 0));
            else if(Input.GetKey(KeyCode.UpArrow))
                Shoot(new Vector2(0, 1));
            else if(Input.GetKey(KeyCode.RightArrow))
                Shoot(new Vector2(1, 0));
            else if(Input.GetKey(KeyCode.DownArrow))
                Shoot(new Vector2(0, -1));

        }
        else
            shotTimer -= Time.deltaTime;
        #endregion

        if(hp <= 0) PlayerDeath();

        if(Input.GetKeyDown(KeyCode.Escape)) Pause();
    }

    private void PlayerMove()
    {
        Vector2 pMove = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        if(pMove.x != 0 || pMove.y != 0) pMove /= Mathf.Sqrt(pMove.x * pMove.x + pMove.y * pMove.y);
        rb.velocity = pMove * playerSpeed;
    }

    private void Shoot(Vector2 direction)
    {
        shotTimer = timeBetweenShots;
        GameObject rBullet = Instantiate(bullet, transform.position, Quaternion.identity);
        rBullet.GetComponent<Bullet>().OnSpawn(direction, bulletDamage, bulletSpeed);
    }

    private void PlayerDeath()
    {
        Debug.Log("Player has died");
        SceneManager.LoadScene(4);
    }

    public void Pause()
    {
        if(!isPaused)
        {
            pauseMenu.SetActive(true);
            isPaused = true;
            Time.timeScale = 0.0f;
        }
        else
        {
            pauseMenu.SetActive(false);
            isPaused = false;
            Time.timeScale = 1.0f;
        }
    }

    public void OnHit(float _damage)
    {
        hp -= _damage;
    }
}