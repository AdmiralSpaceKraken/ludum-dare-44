﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreBoard : MonoBehaviour
{
    public Text s1;
    public Text s2;
    public Text s3;
    public Text s4;
    public Text s5;
    public Text cScore;
    public Text highScore;

    private void Start() 
    {
        Scores.LoadScores();

        if(Scores.currentScore > Scores.score1) highScore.text = "New High Score!";
        else highScore.text = "";

        Scores.RotateScores();
        Scores.SaveScores();

        s1.text = "1: " + Scores.score1;
        s2.text = "2: " + Scores.score2;
        s3.text = "3: " + Scores.score3;
        s4.text = "4: " + Scores.score4;
        s5.text = "5: " + Scores.score5;
        cScore.text = "Your Score: " + Scores.currentScore;
    }
}