﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Spawner : MonoBehaviour
{
    [SerializeField] private float timeToSpawn;
    [SerializeField] private float spawnDifference;
    [SerializeField] private Vector2[] spawnPoints;
    [SerializeField] private float roundTime;
    [SerializeField] private float enemy1HP;

    private static int round;
    private float spawnTimer;
    private float rTimer;

    public Card[] cards;
    public GameObject cardMenu;
    public GameObject enemy1;
    public GameObject enemy2;
    public GameObject player;
    public Text tTimer;
    public Text scoreDisplay;
    public Text hpText;

    private void Start() 
    {
        round = 1;
        rTimer = roundTime / 2;
        Scores.currentScore = 0;
    }

    private void Update() 
    {
        #region enemySpawning
        spawnTimer -= Time.deltaTime;
        if(spawnTimer <= 0)
        {
            int index = Random.Range(0, spawnPoints.Length);
            Vector2 difference = (Vector2)player.transform.position - spawnPoints[index];
            if(Mathf.Abs(difference.x) >= spawnDifference || Mathf.Abs(difference.y) >= spawnDifference)
            {
                int enemyType = Random.Range(0, 3);

                if(round < 2 || enemyType == 0)
                {
                    GameObject rEnemy1 = Instantiate(enemy1, spawnPoints[index], Quaternion.identity);
                    rEnemy1.GetComponent<Enemy>().OnSpawn(player, enemy1HP);
                    spawnTimer = timeToSpawn;
                }
                else
                {
                    GameObject rEnemy2 = Instantiate(enemy2, spawnPoints[index], Quaternion.identity);
                    rEnemy2.GetComponent<Enemy2>().OnSpawn(player, enemy1HP/2);
                    spawnTimer = timeToSpawn;
                }
            }
        }
        #endregion
    
        #region  UIstuff

        tTimer.text = "Time Until Next Mutation: " + Mathf.RoundToInt(rTimer);
        scoreDisplay.text = "Score: " + Scores.currentScore;
        hpText.text = "HP: " + player.GetComponent<Player>().hp;

        #endregion

        rTimer -= Time.deltaTime;
        if(rTimer <= 0)
            NewRound();
    }

    private void NewRound()
    {
        round++;
        Scores.AddScore(100);
        rTimer = roundTime;
        player.GetComponent<Player>().hp += 5;
        cardMenu.SetActive(true);
        Time.timeScale = 0.0f;
        int randCard1 = Random.Range(0, cards.Length);
        int randCard2 = Random.Range(0, cards.Length);
        if(randCard1 == randCard2) 
        {
            randCard1 = Random.Range(0, cards.Length - 1);
            randCard2 = randCard1 + 1;
        }

        cardMenu.GetComponent<CardMenu>().ShowCards(cards[randCard1], cards[randCard2]);

        Debug.Log(cards[randCard1].codeToRun);
        Debug.Log(cards[randCard2].codeToRun);

        timeToSpawn *= 0.75f;
        enemy1HP *= 1.25f;
    }
}