﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Card 
{
    public string name;
    public string descriptionText;
    public int codeToRun;

    private Player player;

    public void RunCode(Player _player)
    {
        player = _player;
        
        if(codeToRun == 1) Code1();
        else if(codeToRun == 2) Code2();
        else if(codeToRun == 3) Code3();
        else if(codeToRun == 4) Code4();
        else if(codeToRun == 5) Code5();
        else if(codeToRun == 6) Code6();
    }

    public void Code1() //Double attack speed, -3 HP
    {
        if(player.hp > 3)
        {
            player.timeBetweenShots /= 2;
            player.hp -= 3;
        }
    }

    public void Code2()
    {
        if(player.hp > 2)
        {
            player.bulletDamage += 2;
            player.hp -= 2;
        }
    }

    public void Code3()
    {
        if(player.hp > 5)
        {
            player.playerSpeed += 2;
            player.hp -= 4;
        }
    }

    public void Code4()
    {
        if(player.hp > 6)
        {
            player.bulletDamage *= 2;
            player.hp -= 6;
        }
    }

    public void Code5()
    {
        if(player.hp > 1)
        {
            player.bulletSpeed += 2;
            player.hp -= 1;
        }
    }

    public void Code6()
    {
        if(player.hp > 8)
        {
            player.playerSpeed *= 2;
            player.hp -= 8;
        }
    }
}