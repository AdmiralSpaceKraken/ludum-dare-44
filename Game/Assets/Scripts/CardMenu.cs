﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardMenu : MonoBehaviour
{
    public Card card1;
    public Card card2;

    public Text c1Title;
    public Text c2Title;
    public Text card1Text;
    public Text card2Text;
    
    public Player player;

    public void ShowCards(Card c1, Card c2)
    {
        card1 = c1;
        card2 = c2;

        c1Title.text = card1.name;
        c2Title.text = card2.name;
        card1Text.text = card1.descriptionText;
        card2Text.text = card2.descriptionText;
    }

    public void OnButtonPress(int cardChosen)
    {
        if(cardChosen == 1)
        {
            card1.RunCode(player);
            Debug.Log(card1.codeToRun);
        }
        else if(cardChosen == 2)
        {
            card2.RunCode(player);
            Debug.Log(card2.codeToRun);
        }

        gameObject.SetActive(false);
        Time.timeScale = 1.0f;
    }
}