﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2 : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private float hp;
    [SerializeField] private LayerMask playerMask;
    [SerializeField] private float attackCooldown;
    [SerializeField] private float damage;
    [SerializeField] private float bulletSpeed;

    private float aTimer; //AttackCooldown timer

    public GameObject player;
    public Rigidbody2D rb;
    public GameObject bullet;

    private void Update() 
    {
        Vector2 movement = (Vector2)player.transform.position - (Vector2)transform.position;
        if(movement.x != 0 || movement.y != 0) movement /= Mathf.Sqrt(movement.x * movement.x + movement.y * movement.y);
        rb.velocity = movement * speed;

        if(hp <= 0)
            Death();

        aTimer -= Time.deltaTime;
        
        if(aTimer <= 0)
        {
            GameObject rBullet = Instantiate(bullet, transform.position, Quaternion.identity);
            rBullet.GetComponent<eBullet>().OnSpawn(movement, damage, bulletSpeed);
            aTimer = attackCooldown;
        }
    }

    private void Death()
    {
        Debug.Log(transform.name + " has died.");
        Scores.AddScore(10);
        Destroy(gameObject);
    }

    public void OnHit(float _damage)
    {
        hp -= _damage;
    }
    
    public void OnSpawn(GameObject _player, float _hp)
    {
        player = _player;
        hp = _hp;
    }
}