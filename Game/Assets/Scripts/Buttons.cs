﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Buttons : MonoBehaviour
{
    public Player player;

    public void Quit()
    {
        Application.Quit();
    }

    public void UnPause()
    {
        player.Pause();
    }

    public void LoadScene(int scene)
    {
        SceneManager.LoadScene(scene);
    }

    public void SetMusicVolume()
    {
        float musicVolume = gameObject.GetComponent<Slider>().value;
        Settings.musicVolume = musicVolume;
    }
}